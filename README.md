User story
============

Once in a long while we get an imcomplete CSS or JS aggregate. We dont know why. The aggregate can be regenerated but browsers may have cached the incomplete file and all future visits are broken until browser cache expires. The long way to remedy this is to deploy a change to the site's CSS or JS. The deployment-free way to do is to merely flush caches, when you are using this module.

Implementation
========
Overrides CSSCollectionRenderer and JsCollectionRenderer services, effectively undoing the change from https://www.drupal.org/project/drupal/issues/3019393. This module Always appends a querystring param whose value changes with each cache clear.
