<?php

namespace Drupal\asset_cache_bust;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Core\Asset\CssCollectionRenderer;

/**
 * Renders CSS assets.
 */
class AssetCachingCSSCollectionRenderer extends CssCollectionRenderer implements AssetCollectionRendererInterface {

  /**
   * {@inheritdoc}
   */
  public function render(array $css_assets) {
    $elements = parent::render($css_assets);

    // A dummy query-string is added to filenames, to gain control over
    // browser-caching. The string changes on every update or full cache
    // flush, forcing browsers to load a new copy of the files, as the
    // URL changed.
    $query_string = $this->state->get('system.css_js_query_string', '0');
    foreach ($elements as $index => $el) {
      if (!UrlHelper::isExternal($el['#attributes']['href'])) {
        $query_string_separator = (strpos($el['#attributes']['href'], '?') !== FALSE) ? '&' : '?';
        $elements[$index]['#attributes']['href'] .= $query_string_separator . $query_string;
      }

    }

    return $elements;
  }

}
