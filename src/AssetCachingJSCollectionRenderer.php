<?php

namespace Drupal\asset_cache_bust;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Core\Asset\JsCollectionRenderer;

/**
 * Renders JS assets.
 */
class AssetCachingJSCollectionRenderer extends JsCollectionRenderer implements AssetCollectionRendererInterface {

  /**
   * {@inheritdoc}
   */
  public function render(array $js_assets) {
    $elements = parent::render($js_assets);

    // A dummy query-string is added to filenames, to gain control over
    // browser-caching. The string changes on every update or full cache
    // flush, forcing browsers to load a new copy of the files, as the
    // URL changed. Files that should not be cached get REQUEST_TIME as
    // query-string instead, to enforce reload on every page request.
    $query_string = $this->state->get('system.css_js_query_string', '0');

    foreach ($elements as $index => $el) {
      if (!empty($el['#attributes']['src']) && !UrlHelper::isExternal($el['#attributes']['src'])) {
        $query_string_separator = (strpos($el['#attributes']['src'], '?') !== FALSE) ? '&' : '?';
        $elements[$index]['#attributes']['src'] .= $query_string_separator . $query_string;
      }
    }

    return $elements;
  }

}
